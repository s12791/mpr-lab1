

public class Address {

private String ulica;

private int nrDomu;

private String miasto;

private String nrTelefonu;

public String getUlica() {

	return ulica;

}

public void setUlica(String ulica) {

	this.ulica = ulica;

}

public int getNrDomu() {

	return nrDomu;

}

public void setNrDomu(int nrDomu) {

	this.nrDomu = nrDomu;

}

public String getMiasto() {

	return miasto;

}

public void setMiasto(String miasto) {

	this.miasto = miasto;

}

public String getNrTelefonu() {

	return nrTelefonu;

}

public void setNrTelefonu(String nrTelefonu) {

	this.nrTelefonu = nrTelefonu;

}

public Address(String ulica, int nrDomu, String miasto, String nrTelefonu) {

	super();

	this.ulica = ulica;

	this.nrDomu = nrDomu;

	this.miasto = miasto;

	this.nrTelefonu = nrTelefonu;

}





}
